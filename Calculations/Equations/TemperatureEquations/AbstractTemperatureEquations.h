#ifndef ABSTRACTTEMPERATUREEQUATIONS_H
#define ABSTRACTTEMPERATUREEQUATIONS_H

class AbstractTemperatureEquations
{
public:
    static double Calculate(double Temperature);
};


#endif // ABSTRACTTEMPERATUREEQUATIONS_H
