#ifndef HEATEQUATION_H
#define HEATEQUATION_H

#include "AbstractTemperatureEquations.h"
#include <cmath>

class HeatEquation: public AbstractTemperatureEquations
{
public:
    static double Calculate(double Temperature)
    {
        return 2.049 + 0.563*pow(10,-3)*Temperature - 0.528*pow(10,5)/pow(Temperature,2);
    }
};

#endif // HEATEQUATION_H
