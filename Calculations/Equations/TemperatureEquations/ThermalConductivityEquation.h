#ifndef THERMALCONDUCTIVITYEQUATION_H
#define THERMALCONDUCTIVITYEQUATION_H

#include "AbstractTemperatureEquations.h"
#include <cmath>

class ThermalConductivityEquation: public AbstractTemperatureEquations
{
public:
    virtual static double Calculate(double Temperature) const
    {
        return 0.134 * 0.1 * (1 + 4.35 * pow(10,-4)*Temperature);
    }
};


#endif // THERMALCONDUCTIVITYEQUATION_H
