#ifndef ABSTRACTTERMINATEEQUATION_H
#define ABSTRACTTERMINATEEQUATION_H

#include "../../../Matrix/AbstractMatrix.h"
#include "../../../Exceptions/NullReferenceException.h"
#include "../../../Exceptions/IncorrectSizeException.h"

class AbstractTerminateEquation
{
public:
    static bool Terminate(AbstractMatrix* PrevIter, AbstractMatrix* CurIter);
};

#endif // ABSTRACTTERMINATEEQUATION_H
