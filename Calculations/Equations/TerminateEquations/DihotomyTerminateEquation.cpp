#include "DihotomyTerminateEquation.h"

static bool Terminate(AbstractMatrix* PrevIter, AbstractMatrix* CurIter)
{
    if (PrevIter == NULL)
        throw NullReferenceException(-1,"PrevIter Matrix is null in DihotomyTerminateEquation.Terminate");

    if (CurIter == NULL)
        throw NullReferenceException(-1,"CurIter Matrix is null in DihotomyTerminateEquation.Terminate");

    int PrevIterHeight = PrevIter->GetHeight();
    int PrevIterWidth = PrevIter->GetWidth();
    int CurIterHeight = CurIter->GetHeight();
    int CurIterWidth = CurIter->GetWidth();

    if (PrevIterHeight != CurIterHeight || PrevIterWidth != CurIterWidth)
        throw IncorrectSizeException(-1,"PrevIter & CurIter Matrix have different sizes in DihotomyTerminateEquation.Terminate");

    double MaxDifference = 0;
    for (int i = 0; i < PrevIterHeight; i++)
        for (int j = 0; j < PrevIterWidth; j++)
        {
            double TempValue = abs(PrevIter->GetElement(i,j) - CurIter->GetElement(i,j));
            if (MaxDifference < TempValue)
                MaxDifference = TempValue;
        }

    return MaxDifference > Constants::GetDihotomyPrecision() ? false : true;
}
