#ifndef DIHOTOMYTERMINATEEQUATION_H
#define DIHOTOMYTERMINATEEQUATION_H


#include "../../../Constants/Constants.h"
#include "AbstractTerminateEquation.h"
#include <cstdlib>


class DihotomyTerminateEquation: public AbstractTerminateEquation
{
public:
    static bool Terminate(AbstractMatrix* PrevIter, AbstractMatrix* CurIter);
};


#endif // DIHOTOMYTERMINATEEQUATION_H
