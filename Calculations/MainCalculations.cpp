#include "MainCalculations.h"

MainCalculations::MainCalculations()
{
}

void MainCalculations::Calculate()
{
    InitYMatrix();
    InitCurYMatrix();

    do
    {
        MoveCurYMatrixToYMatrix();
        // Заполнение большой матрицы
        Calculations.SolveGauss();
        RewriteSolutionToCurYMatrix();
        // Пересчет Lambda
    }
    while (!(DihotomyTerminateEquation::Terminate(&YMatrix,&CurYMatrix)));

}

BigMatrix MainCalculations::GetSolution()
{
    return BigMatrix(YMatrix);
}

void MainCalculations::InitYMatrix()
{
    YMatrix.FillWithValue(TemperatureStart);

}

void MainCalculations::InitCurYMatrix()
{
    CurYMatrix.FillWithValue(TemperatureStart);

}

void MainCalculations::MoveCurYMatrixToYMatrix()
{
    int YHeight = YMatrix.GetHeight();
    int YWidth = YMatrix.GetWidth();
    int CurYHeight = CurYMatrix.GetHeight();
    int CurYWidth = CurYMatrix.GetWidth();

    if (YHeight != CurYHeight || YWidth != CurYWidth)
        throw IncorrectSizeException(-1,"YMatrix & CurYMatrix have different sizes in MainCalculations.MoveCurYMatrixToYMatrix");

    for (int i = 0; i < YHeight; i++)
        for (int j = 0; j < YWidth; j++)
            YMatrix.SetElement(i,j,CurYMatrix.GetElement(i,j));

}

void MainCalculations::RewriteSolutionToCurYMatrix()
{
    int CurYHeight = CurYMatrix.GetHeight();
    int CurYWidth = CurYMatrix.GetWidth();
    int SolutionsCount = Calculations.GetHeight();

    if (CurYHeight*CurYWidth != SolutionsCount)
        throw IncorrectSizeException(-1,"Calculations.height is not equal to number of elements in CurYMatrix in MainCalculations.RewriteSolutionToCurYMatrix");

    int Index = 0;
    int ColumnIndex = Calculations.GetWidth()-1;
    for (int i = 0; i < CurYHeight; i++)
        for (int j = 0; j < CurYWidth; j++)
        {
            CurYMatrix.SetElement(i,j,CurYMatrix.GetElement(Index,ColumnIndex));
            Index++;
        }

}

double MainCalculations::CalculateA(int i, int j)
{
    double X = GetCurrentX(i);
    double AValue = StepX*StepTau / (R1 * R1 * CalculateP(X)*CalculateZ(X)) * CalculateLambdaHeetUsingYMatrix(i,j,i,j-1);
    return AValue;
}

double CalculateB(int i, int j);
double CalculateC(int i, int j);
double CalculateD(int i, int j);
double CalculateE(int i, int j);
double CalculateF(int i, int j);



void MainCalculations::CalculateChangeA()
{
    ChangeA = (M_PI/2 - Delta) / (1 - R/R1);
}

void MainCalculations::CalculateChangeYm()
{
    ChangeYm = tan(M_PI/2 - Delta);
}

double MainCalculations::CalculateP(double X)
{
    double P = ChangeA/ChangeYm * (1+pow((X-1)*ChangeYm,2));
    return P;
}

double MainCalculations::CalculateZ(double X)
{
    double Z = 1 + 1 / ChangeA * atan((X-1)*ChangeYm);
    return Z;
}

double MainCalculations::GetCurrentX(int index)
{
    double X = StartX + index*StepX;
    return X;
}

double MainCalculations::GetCurrentTau(int index)
{
    double Tau = StartTau + index*StepTau;
}

double MainCalculations::GetCurrentPhi(int index)
{
    double Phi = StartPhi + index * StepPhi;
    return Phi;
}

double MainCalculations::CalculateR(double X)
{
    double Z = CalculateZ(X);
    double CurR = R1*Z;
    return CurR;

}

double MainCalculations::RecalculateLambdaUsingYMatrix()
{
    int LambdaHeight = LambdaHeat.GetHeight();
    int LambdaWidth = LambdaHeat.GetWidth();
    int YHeight = YMatrix.GetHeight();
    int YWidth = YMatrix.GetWidth();

    if (YHeight != LambdaHeight || YWidth != LambdaWidth)
        throw IncorrectSizeException(-1,"LambdaHeet size is not equal to YMatrix size in MainCalculations.RecalculateLambdaUsingYMatrix");

    for (int i = 0; i < LambdaHeight; i++)
        for (int j = 0; j < LambdaWidth; j++)
            LambdaHeat.SetElement(i,j,HeatEquation::Calculate(YMatrix.GetElement(i,j)));
}

double MainCalculations::CalculateLambdaHeetUsingYMatrix(int IndexI1, int IndexJ1, int IndexI2, int IndexJ2)
{
    double MediumTemperature = 0.5 * (YMatrix.GetElement(IndexI1,IndexJ1) + YMatrix.GetElement(IndexI2,IndexJ2));
    double LambdaHeatValue = HeatEquation::Calculate(MediumTemperature);
    return LambdaHeatValue;

}
