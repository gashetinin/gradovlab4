#ifndef MAINCALCULATIONS_H
#define MAINCALCULATIONS_H

#include "../Matrix/BigMatrix.h"
#include "Equations/TerminateEquations/AbstractTerminateEquation.h"
#include "Equations/TerminateEquations/DihotomyTerminateEquation.h"
#include "Equations/TemperatureEquations/HeatEquation.h"

#include <cmath>

class MainCalculations
{
protected:
    BigMatrix YMatrix;
    BigMatrix CurYMatrix;
    BigMatrix LambdaHeat;
    BigMatrix Calculations;

    // Координаты и их шаги
    double StartX;
    double FinishX;
    double StepX;
    double GetCurrentX(int index);

    double StartTau;
    double FinishTau;
    double StepTau;
    double GetCurrentTau(int index);

    double StartPhi;
    double FinishPhi;
    double StepPhi;
    double GetCurrentPhi(int index);

    // Радиусы
    double R;
    double R1;

    // Параметры сетки и доп.переменные
    double Delta;
    double ChangeA;
    double ChangeYm;


    // Начальная температура
    double TemperatureStart;


    // Заполнение
    void InitYMatrix();
    void InitCurYMatrix();
    void MoveCurYMatrixToYMatrix();
    void RewriteSolutionToCurYMatrix();

    // Вычисление коэффициентов уравнения
    double CalculateA(int i, int j);
    double CalculateB(int i, int j);
    double CalculateC(int i, int j);
    double CalculateD(int i, int j);
    double CalculateE(int i, int j);
    double CalculateF(int i, int j);

    // Вычисление доп.переменных
    void CalculateChangeA();
    void CalculateChangeYm();

    // Пересчет доп. переменных
    double CalculateP(double X);
    double CalculateZ(double X);
    double CalculateR(double X);

    // Подсчет lambda и вычисления в промежуточных узлах
    double RecalculateLambdaUsingYMatrix();
    double CalculateLambdaHeetUsingYMatrix(int IndexI1, int IndexJ1, int IndexI2, int IndexJ2);


public:
    MainCalculations();

    void Calculate();
    BigMatrix GetSolution();


};



#endif // MAINCALCULATIONS_H
