#ifndef CONSTANTS_H
#define CONSTANTS_H



class Constants{

protected:
    static constexpr double MatrixPrecision = 0.0001;
    static constexpr double DihotomyPrecision = 0.0001;

public:
    static double GetMatrixPrecision() { return MatrixPrecision; }
    static double GetDihotomyPrecision() { return DihotomyPrecision; }
};

#endif // CONTANTS_H
