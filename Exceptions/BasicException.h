#ifndef BASICEXCEPTION_H
#define BASICEXCEPTION_H


#include <new>          // std::bad_alloc
#include <cstring>

class BasicException
{
protected:
    int ErrorCode;
    char* Message;

public:
    char* GetMessage() const
    {
        char* MessageText = NULL;
        if (Message)
        {
            try
            {
                MessageText = new char[strlen(Message)+1];
                strcpy(MessageText,Message);
            }
            catch (std::bad_alloc& ba) { }
        }
        return MessageText;
    }
    int GetErrorCode() const
    {
        return ErrorCode;
    }

};

#endif // BASICEXCEPTION_H
