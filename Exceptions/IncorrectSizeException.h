#ifndef INCORRECTSIZEEXCEPTION_H
#define INCORRECTSIZEEXCEPTION_H

#include "BasicException.h"

class IncorrectSizeException: public BasicException
{
public:
    IncorrectSizeException()
    {
        ErrorCode = 0;
        Message = NULL;
    }
    IncorrectSizeException(int nCode, char* nMessage)
    {
        ErrorCode = nCode;
        Message = NULL;
        if (nMessage)
        {
            try
            {
                Message = new char[strlen(nMessage)+1];
                strcpy(Message,nMessage);
            }
            catch (std::bad_alloc& ba) { }
        }


    }
    IncorrectSizeException(const IncorrectSizeException& ex)
    {
        ErrorCode = ex.ErrorCode;
        Message = NULL;
        if (ex.Message)
        {
            try
            {
                Message = new char[strlen(ex.Message)+1];
                strcpy(Message,ex.Message);
            }
            catch (std::bad_alloc& ba) { }
        }


    }
    ~IncorrectSizeException()
    {
        if (Message)
            delete[] Message;
    }
};


#endif // INCORRECTSIZEEXCEPTION_H
