#ifndef NOSOLUTIONEXCEPTION_H
#define NOSOLUTIONEXCEPTION_H

#include "BasicException.h"

class NoSolutionException: public BasicException
{
public:
    NoSolutionException()
    {
        ErrorCode = 0;
        Message = NULL;
    }
    NoSolutionException(int nCode, char* nMessage)
    {
        ErrorCode = nCode;
        Message = NULL;
        if (nMessage)
        {
            try
            {
                Message = new char[strlen(nMessage)+1];
                strcpy(Message,nMessage);
            }
            catch (std::bad_alloc& ba) { }
        }


    }
    NoSolutionException(const NoSolutionException& ex)
    {
        ErrorCode = ex.ErrorCode;
        Message = NULL;
        if (ex.Message)
        {
            try
            {
                Message = new char[strlen(ex.Message)+1];
                strcpy(Message,ex.Message);
            }
            catch (std::bad_alloc& ba) { }
        }


    }
    ~NoSolutionException()
    {
        if (Message)
            delete[] Message;
    }
};


#endif // NOSOLUTIONEXCEPTION_H
