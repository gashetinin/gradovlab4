#ifndef NULLREFERENCEEXCEPTION_H
#define NULLREFERENCEEXCEPTION_H

#include "BasicException.h"

class NullReferenceException: public BasicException
{
public:
    NullReferenceException()
    {
        ErrorCode = 0;
        Message = NULL;
    }
    NullReferenceException(int nCode, char* nMessage)
    {
        ErrorCode = nCode;
        Message = NULL;
        if (nMessage)
        {
            try
            {
                Message = new char[strlen(nMessage)+1];
                strcpy(Message,nMessage);
            }
            catch (std::bad_alloc& ba) { }
        }


    }
    NullReferenceException(const NullReferenceException& ex)
    {
        ErrorCode = ex.ErrorCode;
        Message = NULL;
        if (ex.Message)
        {
            try
            {
                Message = new char[strlen(ex.Message)+1];
                strcpy(Message,ex.Message);
            }
            catch (std::bad_alloc& ba) { }
        }


    }
    ~NullReferenceException()
    {
        if (Message)
            delete[] Message;
    }
};


#endif // NULLREFERENCEEXCEPTION_H
