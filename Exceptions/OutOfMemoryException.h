#ifndef OUTOFMEMORYEXCEPTION_H
#define OUTOFMEMORYEXCEPTION_H

#include "BasicException.h"

class OutOfMemoryException: public BasicException
{
public:
    OutOfMemoryException()
    {
        ErrorCode = 0;
        Message = NULL;
    }
    OutOfMemoryException(int nCode, char* nMessage)
    {
        ErrorCode = nCode;
        Message = NULL;
        if (nMessage)
        {
            try
            {
                Message = new char[strlen(nMessage)+1];
                strcpy(Message,nMessage);
            }
            catch (std::bad_alloc& ba) { }
        }


    }
    OutOfMemoryException(const OutOfMemoryException& ex)
    {
        ErrorCode = ex.ErrorCode;
        Message = NULL;
        if (ex.Message)
        {
            try
            {
                Message = new char[strlen(ex.Message)+1];
                strcpy(Message,ex.Message);
            }
            catch (std::bad_alloc& ba) { }
        }


    }
    ~OutOfMemoryException()
    {
        if (Message)
            delete[] Message;
    }
};

#endif // OUTOFMEMORYEXCEPTION_H
