#ifndef OUTOFRANGEEXCEPTION_H
#define OUTOFRANGEEXCEPTION_H


class OutOfRangeException: public BasicException
{
public:
    OutOfRangeException()
    {
        ErrorCode = 0;
        Message = NULL;
    }
    OutOfRangeException(int nCode, char* nMessage)
    {
        ErrorCode = nCode;
        Message = NULL;
        if (nMessage)
        {
            try
            {
                Message = new char[strlen(nMessage)+1];
                strcpy(Message,nMessage);
            }
            catch (std::bad_alloc& ba) { }
        }


    }
    OutOfRangeException(const OutOfRangeException& ex)
    {
        ErrorCode = ex.ErrorCode;
        Message = NULL;
        if (ex.Message)
        {
            try
            {
                Message = new char[strlen(ex.Message)+1];
                strcpy(Message,ex.Message);
            }
            catch (std::bad_alloc& ba) { }
        }


    }
    ~OutOfRangeException()
    {
        if (Message)
            delete[] Message;
    }
};


#endif // OUTOFRANGEEXCEPTION_H
