#-------------------------------------------------
#
# Project created by QtCreator 2016-11-02T23:48:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Matrix/BigMatrix.cpp \
    Calculations/MainCalculations.cpp \
    Calculations/Interpolation/AbsorbCoefficient.cpp \
    maincalculations.cpp \
    Calculations/Equations/TerminateEquations/DihotomyTerminateEquation.cpp

HEADERS  += mainwindow.h \
    matrix.h \
    Matrix/BigMatrix.h \
    Matrix/AbstractMatrix.h \
    Exceptions/OutOfMemoryException.h \
    Exceptions/BasicException.h \
    Exceptions/OutOfRangEexception.h \
    Exceptions/NoSolutionException.h \
    Constants/Constants.h \
    Calculations/MainCalculations.h \
    Calculations/Interpolation/AbstractInterpolation.h \
    Calculations/Interpolation/AbsorbCoefficient.h \
    Calculations/EdgeEquations/AbstractEdgeEquation.h \
    Calculations/Equations/EdgeEquations/AbstractEdgeEquation.h \
    Calculations/Equations/TerminateEquations/AbstractTerminateEquation.h \
    Calculations/Equations/TerminateEquations/DihotomyTerminateEquation.h \
    Exceptions/IncorrectSizeException.h \
    Exceptions/NullReferenceException.h \
    Calculations/Equations/TemperatureEquations/ThermalConductivityEquation.h \
    Calculations/Equations/TemperatureEquations/HeatEquation.h \
    Calculations/Equations/TemperatureEquations/AbstractTemperatureEquations.h

FORMS    += mainwindow.ui

DISTFILES += \
    Exceptions/Untitled Document \
    Calculations/Untitled Document \
    Calculations/Untitled Document \
    Calculations/Interpolation/1 \
    Calculations/EdgeEquations/1 \
    Calculations/Equations/EdgeEquations/1 \
    Calculations/Equations/TerminateEquations/1
