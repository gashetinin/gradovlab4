#ifndef ABSTRACTMATRIX_H
#define ABSTRACTMATRIX_H

#include <new>          // std::bad_alloc
#include "../Exceptions/OutOfMemoryException.h"
#include "../Exceptions/OutOfRangEexception.h"

class AbstractMatrix
{
protected:
    int Width;
    int Height;
    virtual void AllocateMemory() = 0;
    virtual void FreeMemory() = 0;

public:
    int GetWidth() const
    {
        return Width;
    }

    int GetHeight() const
    {
        return Height;
    }

    virtual void SetElement(int,int,double) = 0;
    virtual double GetElement(int,int) const = 0;
    virtual void SolveGauss() = 0;
    virtual void FillWithValue(double Value) = 0;


};

#endif // ABSTRACTMATRIX_H
