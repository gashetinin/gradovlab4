#include "BigMatrix.h"


BigMatrix::BigMatrix()
{
    Width = 0;
    Height = 0;
    Values = NULL;
}

BigMatrix::BigMatrix(int nWidth, int nHeight)
{
    Width = nWidth;
    Height = nHeight;
    AllocateMemory();
}

BigMatrix::BigMatrix(const BigMatrix& BM)
{
   Height = BM.Height;
   Width = BM.Width;
   AllocateMemory();

   for (int i = 0; i < Height; i++)
       for (int j = 0; j < Width; j++)
           SetElement(i,j,BM.GetElement(i,j));
}

BigMatrix::~BigMatrix()
{
    FreeMemory();
}

void BigMatrix::AllocateMemory()
{
    try
    {
        Values = NULL;
        Values = new double*[Height];
        for (int i = 0; i < Height; i++)
            Values[i] = NULL;

        for (int i = 0; i < Height; i++)
            Values[i] = new double[Width];
    }
    catch (std::bad_alloc& ba)
    {
        FreeMemory();
        throw OutOfMemoryException(-1,"bad_alloc exception in AllocateMemory_BigMatrix");
    }

}

void BigMatrix::FreeMemory()
{
    if (Values)
    {
        for (int i = 0; i < Height; i++)
            if (Values[i])
                delete[] Values;
        delete[] Values;
    }
}

void BigMatrix::SetElement(int HeightIndex, int WidthIndex, double newValue)
{
    if (HeightIndex >= 0 && HeightIndex < Height && WidthIndex >= 0 && WidthIndex < Width)
        Values[HeightIndex][WidthIndex] = newValue;
    else
        throw OutOfRangeException(-1,"Index out of range in SetElement in BigMatrix");
}

double BigMatrix::GetElement(int HeightIndex, int WidthIndex) const
{
    if (HeightIndex >= 0 && HeightIndex < Height && WidthIndex >= 0 && WidthIndex < Width)
        return Values[HeightIndex][WidthIndex];
    else
        throw OutOfRangeException(-1,"Index out of range in GetElement in BigMatrix");


}

void BigMatrix::SolveGauss()
{
    double MaxElement = 0;
    int K = 0, Index;

    K = 0;
    while (K < Height) {
    // Поиск строки с максимальным a[i][k]
        MaxElement = abs(Values[K][K]);
        Index = K;
        for (int i = K + 1; i < Height; i++) {
            if (abs(Values[i][K]) > MaxElement) {
                MaxElement = abs(Values[i][K]);
                Index = i;
            }
        }
      // Перестановка строк
      if (MaxElement < Constants::GetMatrixPrecision())
          throw NoSolutionException(-1,"No solution in Gauss method");
      for (int j = 0; j < Width; j++) {
          double temp = Values[K][j];
          Values[K][j] = Values[Index][j];
          Values[Index][j] = temp;
      }

      // Нормализация уравнений
      for (int i = K; i < Height; i++) {
          double temp = Values[i][K];
          if (abs(temp) < Constants::GetMatrixPrecision()) continue; // для нулевого коэффициента пропустить
          for (int j = 0; j < Width; j++)
            Values[i][j] = Values[i][j] / temp;

          if (i == K)  continue; // уравнение не вычитать само из себя
          for (int j = 0; j < Width; j++)
            Values[i][j] = Values[i][j] - Values[K][j];
        }
        K++;
    }

    // обратная подстановка
    for (K = Height - 1; K >= 0; K--)
        for (int i = 0; i < K; i++)
            Values[i][Width-1] = Values[i][Width-1] - Values[i][K] * Values[K][Width-1];
}

void BigMatrix::FillWithValue(double Value)
{
    for (int i = 0; i < Height; i++)
        for (int j = 0; j < Width; j++)
            Values[i][j] = Value;
}

BigMatrix& BigMatrix::operator= (const BigMatrix& BM)
{
    Height = BM.Height;
    Width = BM.Width;
    AllocateMemory();

    for (int i = 0; i < Height; i++)
        for (int j = 0; j < Width; j++)
            SetElement(i,j,BM.GetElement(i,j));
    return *this;
}
