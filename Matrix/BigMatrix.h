#ifndef MATRIX_H
#define MATRIX_H

#include "../Exceptions/NoSolutionException.h"

#include "AbstractMatrix.h"
#include "../Constants/Constants.h"
#include <cstdlib>

// Стандартное хранение матрицы
class BigMatrix: public AbstractMatrix
{
protected:
    double** Values;
    void AllocateMemory();
    void FreeMemory();



public:
    BigMatrix();
    BigMatrix(int nWidth, int nHeight);
    BigMatrix(const BigMatrix&);
    ~BigMatrix();

   BigMatrix& operator= (const BigMatrix& BM);

    void SetElement(int,int,double);
    double GetElement(int,int) const;
    void SolveGauss();
    void FillWithValue(double Value);

};


#endif // MATRIX_H
